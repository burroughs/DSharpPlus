﻿namespace DSharpPlus
{
    public enum MentionType
    {
        None        = 0,
        Username    = 1,
        Nickname    = 2,
        Channel     = 4,
        Role        = 8
    }
}
