﻿namespace DSharpPlus
{
    public enum LogLevel
    {
        Unnecessary = 0,
        Debug       = 1,
        Info        = 2,
        Warning     = 4,
        Error       = 8,
        Critical    = 16
    }
}
