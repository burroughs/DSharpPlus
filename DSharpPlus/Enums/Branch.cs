﻿namespace DSharpPlus
{
    public enum Branch
    {
        Stable  = 0,
        PTB     = 1,
        Canary  = 2
    }
}
